import os
import shutil
import time
from functools import cached_property
from tempfile import mkdtemp

from pyzotero import zotero as pyzotero
from dotenv import load_dotenv


class Zotero:
    def __init__(self, debugging: bool = False):
        self.debugging = debugging
        self.api = self._load_api()
        self.key_mapping = self._load_key_mapping()
        self.tmp_download_path = mkdtemp()

        if self.debugging:
            print(f"🐛  Created temp dir for zotero: {self.tmp_download_path}")

    def __del__(self):
        shutil.rmtree(self.tmp_download_path)

        if self.debugging:
            print(f"🐛  Deleted temp dir for zotero: {self.tmp_download_path}")

    def _load_api(self):
        load_dotenv()
        library_id = os.getenv('LIBRARY_ID')
        api_key = os.getenv('API_KEY')
        library_type = 'user'

        return pyzotero.Zotero(library_id, library_type, api_key)

    def _load_key_mapping(self):
        pass

    @cached_property
    def papers(self):
        zotero_keys = dict()

        for item in self.api.top():
            try:
                lastname = item['data']['creators'][0]['lastName']
                year = item['data']['date'][:4]  # FIXME: This is not the most reliable way to get the year

                paper_key = str(lastname + year).lower()
                zotero_key = item['key']
                zotero_keys[paper_key] = zotero_key

            except KeyError:
                print(f"Could not read authors on {item}")

        return zotero_keys

    def _get_zotero_key(self, paper_key: str):
        if paper_key not in self.papers:
            raise FileNotFoundError(f"Could not find {paper_key} on Zotero")

        zotero_key = self.papers[paper_key]

        return zotero_key

    def upload_pdf_to_zotero(self, paper_key: str, pdf_path: str):
        zotero_key = self._get_zotero_key(paper_key)

        self.api.attachment_simple([pdf_path], zotero_key)

    def delete_pdfs(self, paper_key: str):
        zotero_key = self._get_zotero_key(paper_key)

        items_to_be_deleted = []
        for item in self.api.children(zotero_key):
            if is_pdf(item):
                items_to_be_deleted.append(item)

        if items_to_be_deleted:
            lm_v = self.api.last_modified_version()
            self.api.delete_item(items_to_be_deleted, lm_v)

    def overwrite_pdf(self, paper_key: str, pdf_path: str):
        self.delete_pdfs(paper_key)
        self.upload_pdf_to_zotero(paper_key, pdf_path)

    def download(self, paper_key: str):
        zotero_key = self._get_zotero_key(paper_key)
        children = self.api.children(zotero_key)

        paper_pdfs = [child for child in children if is_pdf(child)]

        assert len(paper_pdfs) < 2, f"{paper_key} has multiple PDFs"

        if len(paper_pdfs) == 0:
            raise FileNotFoundError(f"Could not find a PDF for {paper_key}")

        pdf_key = paper_pdfs[0]['key']
        pdf_name = paper_key + ".pdf"

        self.api.dump(pdf_key, pdf_name, self.tmp_download_path)

        return os.path.join(self.tmp_download_path, pdf_name)


def is_pdf(zotero_item: dict) -> bool:
    data = zotero_item['data']

    is_attachment = 'itemType' in data and data['itemType'] == 'attachment'
    is_pdf = 'contentType' in data and data['contentType'] == 'application/pdf'

    return is_attachment and is_pdf