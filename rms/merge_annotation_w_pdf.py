import os
import shutil
import zipfile

from PyPDF4 import PdfFileReader, PdfFileWriter


def extract_pdf_from_rm_zip(zip_path: str, destination: str = None) -> str:
    """

    Args:
        zip_path:
        destination:
            The path to the folder where the extracted pdf should be placed.
            If None then the folder of the zip is used.

    Returns:
        The path of the extracted pdf.
    """
    if not destination:
        destination = os.path.dirname(zip_path)

    pdf_filename = os.path.basename(zip_path)[:-4] + "-full.pdf"

    destination = os.path.join(destination, pdf_filename)
    with zipfile.ZipFile(zip_path, 'r') as archive:
        file_list = [f.filename for f in archive.filelist]
        pdf_files = [f for f in file_list if '.pdf' in f]

        assert len(pdf_files) == 1, \
            "The remarkable zip file does not have exactly one pdf file."

        pdf_filename = pdf_files[0]

        source = archive.open(pdf_filename)
        target = open(destination, "wb")
        with source, target:
            shutil.copyfileobj(source, target)

    return destination


def extract_annotation_mapping_from_rm_zip(zip_path: str):
    with zipfile.ZipFile(zip_path, 'r') as archive:
        file_list = [f.filename for f in archive.filelist]
        json_files = [f for f in file_list if '.json' in f]

        # The annotation PDF from rmapi extraction only contains the
        # files that has an annotation. This list contains each of
        # annotation pages index in the full pdf.
        annotation_mapping = [int(f.split("/")[-1].split("-")[0]) for f in json_files]

    return annotation_mapping


def merge(zip_path: str,
          annotation_pdf_path: str,
          destination: str = None):

    full_pdf_path = extract_pdf_from_rm_zip(zip_path)
    annotation_to_full_idx = extract_annotation_mapping_from_rm_zip(zip_path)

    if not destination:
        destination = os.path.basename(full_pdf_path)[:-8] + "final.pdf"

    annotation_pdf_reader = PdfFileReader(annotation_pdf_path)
    assert annotation_pdf_reader.numPages == len(annotation_to_full_idx), \
        "The number of annotation pages and their mapping do not match"

    full_pdf_reader = PdfFileReader(full_pdf_path)

    writer = PdfFileWriter()
    # TODO: We are not getting the outline as a part of the final pdf
    # writer.cloneReaderDocumentRoot(full_pdf_reader)

    for page_idx in range(full_pdf_reader.numPages):

        # Use the annotated page if it exists
        if page_idx in annotation_to_full_idx:
            annotation_pdf_idx = annotation_to_full_idx.index(page_idx)
            writer.addPage(annotation_pdf_reader.getPage(annotation_pdf_idx))

        else:
            writer.addPage(full_pdf_reader.getPage(page_idx))

    assert writer.getNumPages() == full_pdf_reader.getNumPages(), \
        "The generated PDF do not have the same number of pages as the original"

    for name, dest in full_pdf_reader.getNamedDestinations().items():
        writer.addNamedDestinationObject(dest)

    with open(destination, "wb") as out_file:
        writer.write(out_file)

    return destination
