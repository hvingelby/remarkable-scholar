import os
from os.path import join
import shutil
from functools import cached_property
from subprocess import check_output
from tempfile import mkdtemp

from merge_annotation_w_pdf import merge


class Remarkable:

    def __init__(self, debugging=False):
        self.debugging = debugging
        self.rm_paper_folder = "Papers"
        self.rmapi_path = ""

        self.tmp_download_path = mkdtemp()

        if self.debugging:
            print(f"🐛  Created temp dir for remarkable: {self.tmp_download_path}")

    def __del__(self):
        shutil.rmtree(self.tmp_download_path)

        if self.debugging:
            print(f"🐛  Deleted temp dir for remarkable: {self.tmp_download_path}")

    @cached_property
    def papers(self):
        raw_output = check_output([self.rmapi_path, 'ls', self.rm_paper_folder])

        files = raw_output.decode('utf8').split("\n")
        files = [f.split("\t")[1] for f in files if f and f.split("\t")[0]=="[f]"]

        return files

    def _update_papers(self):
        if 'papers':
            del rm.__dict__["papers"]

    def download(self, paper: str, destination: str = None):
        if paper not in self.papers:
            raise FileNotFoundError(f"❌  {paper} does not exist on remarkable")

        cwd = os.path.abspath(os.getcwd())
        zip_file = f"{paper}.zip"
        zip_path = join(self.tmp_download_path, zip_file)

        pdf_file = f"{paper}-annotations.pdf"
        pdf_path = join(self.tmp_download_path, pdf_file)

        # Download pdf and annotations from remarkable
        rm_paper_path = os.path.join(self.rm_paper_folder, paper)
        check_output([self.rmapi_path, 'geta', rm_paper_path])

        # Move files from working dir to tmp dir
        shutil.move(join(cwd, zip_file), zip_path)
        shutil.move(join(cwd, pdf_file), pdf_path)

        # Merge full pdf and annotation pdf
        if not destination:
            destination = os.path.join(self.tmp_download_path, paper)

        destination += ".pdf"
        merge(zip_path, pdf_path, destination)

        return destination

    def upload_papers(self, paper_paths: list):
        """
        Upload a list of papers to the remarkable
        Args:
            paper_paths:
        """
        for paper_path in paper_paths:
            filename = os.path.basename(paper_path)
            paper_name = filename[:-4] if filename[-4:] == ".pdf" else filename

            # TODO: Move this out of the loop?
            if paper_name in self.papers:
                raise FileExistsError(f"{paper_name} already exists on RM")

            t = check_output([
                self.rmapi_path,
                'put',
                paper_path,
                f"/{self.rm_paper_folder}"
            ])
            # TODO: check if upload was a success?
