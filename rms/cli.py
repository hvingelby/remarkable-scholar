import os

import click

from rms.remarkable import Remarkable
from rms.zotero import Zotero


@click.command()
@click.argument('papers', nargs=-1)
@click.option('--debug', '-d', is_flag=True, help="Output debugging info")
@click.option('--all', '-a', is_flag=True, help="Synchronizes all papers "
                                                "from remarkable to zotero")
@click.version_option("1.0.0", prog_name="Remarkable Scholar")
def main(papers, debug, all):
    if debug:
        print("Remarkable Scholar is running in debugging mode\n")

    rm = Remarkable(debugging=debug)
    z = Zotero(debugging=debug)

    for paper in papers:
        if paper not in z.papers:
            print(f"⚠️  {paper} does not exist in Zotero")
            continue

        if paper in rm.papers:
            send_from_rm_to_zotero(paper, debugging=debug)
        else:
            send_from_zotero_to_rm(paper, debugging=debug)


def send_from_rm_to_zotero(paper: str, debugging: bool = False):
    # Try to download from RM and send to zotero
    rm = Remarkable()
    z = Zotero()

    dl_pdf_path = rm.download(paper)
    if debugging:
        print(f"🐛  Downloaded {paper} from remarkable to: {dl_pdf_path}")

    z.overwrite_pdf(paper, dl_pdf_path)
    if debugging:
        print(f"🐛  Uploaded {paper} from temp dir to zotero")

    os.remove(dl_pdf_path)
    print(f"✅  {paper} was synced from remarkable to zotero")


def send_from_zotero_to_rm(paper: str, debugging: bool = False):

    rm = Remarkable()
    z = Zotero()

    pdf_path = z.download(paper)
    if debugging:
        print(f"🐛  Downloaded {paper} from zotero to: {pdf_path}")

    rm.upload_papers([pdf_path])
    if debugging:
        print(f"🐛  Uploaded {paper} from temp dir to remarkable")

    print(f"✅  {paper} was transferred from zotero to remarkable")


if __name__ == '__main__':
    main()
