from setuptools import setup

setup(
    name='remarkable-scholar',
    description='A CLI for synchronizing reMarkable with Zotero',
    author='Rasmus Hvingelby',
    author_email='r.hvingelby@gmail.com',
    version='0.0.1',
    packages=['rms'],
    entry_points={
        'console_scripts': [
            'rms=rms:main'
        ]},
    license='MIT',
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Scientific/Engineering",
    ]
)